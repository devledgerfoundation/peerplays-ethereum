const {ethers, upgrades} = require("hardhat");
const Web3 = require('web3');
var currentProvider = new Web3.providers.HttpProvider('http://localhost:8545');
const web3 = new Web3(currentProvider);

async function main() {
    const ProxyAddress = "0x572eA65762bFFf521C11Ce5334FfaaF4bDD4974e";
    const ProxyAdminAddress = "0xB3c94Ee98037982bA2684F2d0c32A5f87d9FBBCA";
    const PrimaryWalletV2 = await ethers.getContractFactory("PrimaryWalletV2");
    const PrimaryWalletV2Address = await upgrades.prepareUpgrade(ProxyAddress, PrimaryWalletV2);    
    console.log('PrimaryWalletV2Address:', PrimaryWalletV2Address);

    var abi = JSON.parse('[ { "inputs": [ { "internalType": "address", "name": "_proxyAddress", "type": "address" }, { "internalType": "address", "name": "_implementationAddress", "type": "address" }, { "internalType": "string", "name": "_version", "type": "string" } ], "name": "upgrade", "outputs": [], "stateMutability": "nonpayable", "type": "function" } ]');
    var ContractInstance = new web3.eth.Contract(abi, ProxyAdminAddress);

    //! Here we should call upgrade for all active SONs
    const OwnerAddress = "0x5FbBb31BE52608D2F52247E8400B7fCaA9E0bC12";
    await ContractInstance.methods.upgrade(ProxyAddress, PrimaryWalletV2Address, "v2").send( { "to": ProxyAdminAddress, "from": OwnerAddress }, function (err, res) {
        if (err) {
        console.log("An error occured", err)
        return
        }
        console.log("upgrade:", res)
    })

}

main()
        .then(() => process.exit(0))
        .catch((error) => {
            console.error(error);
            process.exit(1);
        });
