const {expect} = require("chai");
const {ethers, upgrades} = require("hardhat");

describe("PrimaryWallet contract", function () {

    let Owner;

    let PrimaryWalletContract;
    let ProxyAdminAddress;

    it("Deployment", async function () {
        const [owner] = await ethers.getSigners();
        Owner = owner;

        const PrimaryWallet = await ethers.getContractFactory("PrimaryWallet");
        PrimaryWalletContract = await upgrades.deployProxy(PrimaryWallet, [Owner.address]);
        await PrimaryWalletContract.deployed();

        ProxyAdminAddress = await PrimaryWalletContract.getProxyAdmin();        
        await upgrades.admin.changeProxyAdmin(PrimaryWalletContract.address, ProxyAdminAddress);

        expect(
            (await PrimaryWalletContract.address)
        ).not.equal(0);

        expect(
            (await ProxyAdminAddress)
        ).not.equal(0);
    });

    it("Balance should be zero", async function () {
        expect(
            (await PrimaryWalletContract.getBalance())
        ).to.equal(0);
    });

    it("Owner should be default account", async function () {
        expect(
            (await PrimaryWalletContract.getOwner())
        ).to.equal(Owner.address);
    });

    it("getOwners should return empty array", async function () {
        expect(
            (await PrimaryWalletContract.getOwners()).length
        ).to.equal(0);
    });

    it("getBlockNumber should work", async function () {
        expect(
            (await PrimaryWalletContract.getBlockNumber())
        ).to.equal(await ethers.provider.getBlockNumber());
    });

    it("Version before upgrade should be v1", async function () {
        expect(
            (await PrimaryWalletContract.getVersion())
        ).to.equal("v1");
    });

    it("Balance should increase after sendTransaction", async function () {
        const tx = {
            to: PrimaryWalletContract.address,
            value: ethers.utils.parseEther("1.0")
        };
        await Owner.sendTransaction(tx);

        expect(
            (await PrimaryWalletContract.getBalance())
        ).to.equal(ethers.utils.parseEther("1.0"));
    });

    it("Balance should increase after deposit function", async function () {
        const options = {value: ethers.utils.parseEther("2.0")};
        await PrimaryWalletContract.connect(Owner).deposit(options);

        expect(
            (await PrimaryWalletContract.getBalance())
        ).to.equal(ethers.utils.parseEther("3.0"));
    });

    it("Balance should decrease after withdraw function", async function () {
        const object_id = "1.36.0";

        await PrimaryWalletContract.connect(Owner)['withdraw(address,uint256,string)'](Owner.address, ethers.utils.parseEther("1.0"), object_id);

        expect(
            (await PrimaryWalletContract.getBalance())
        ).to.equal(ethers.utils.parseEther("2.0"));
    });

    it("Balance should decrease after withdraw function (single transaction)", async function () {
        const transactions = [{data:"0xe088747b0000000000000000000000005fbbb31be52608d2f52247e8400b7fcaa9e0bc120000000000000000000000000000000000000000000000000de0b6b3a764000000000000000000000000000000000000000000000000000000000000000000600000000000000000000000000000000000000000000000000000000000000006312E33362E310000000000000000000000000000000000000000000000000000", signature:{v:27, r:"0xa65ca8e201372243fd9525bb35e39208ced49c7d7fd1066fe0c5e4dac2a0b0f3", s:"0x78188243fba0857704aa9fc4e12292e3cb96bd97429c109080036c45e57ee84b"}}];

        await PrimaryWalletContract.connect(Owner)['withdraw((bytes,(uint8,bytes32,bytes32))[])'](transactions);

        expect(
            (await PrimaryWalletContract.getBalance())
        ).to.equal(ethers.utils.parseEther("1.0"));
    });

    it("Upgrade should be successful", async function () {
        const PrimaryWalletV2 = await ethers.getContractFactory("PrimaryWalletV2");
        const PrimaryWalletV2Address = await upgrades.prepareUpgrade(PrimaryWalletContract.address, PrimaryWalletV2);  

        const abi = JSON.parse('[ { "inputs": [ { "internalType": "address", "name": "_proxyAddress", "type": "address" }, { "internalType": "address", "name": "_implementationAddress", "type": "address" }, { "internalType": "string", "name": "_version", "type": "string" } ], "name": "upgrade", "outputs": [], "stateMutability": "nonpayable", "type": "function" } ]');
        const ContractInstance = new ethers.Contract(ProxyAdminAddress, abi, Owner);

        await ContractInstance.upgrade(PrimaryWalletContract.address, PrimaryWalletV2Address, "v2");
    });

    it("Version after upgrade should be v2", async function () {
        expect(
            (await PrimaryWalletContract.getVersion())
        ).to.equal("v2");
    });

    it("New function (test) should appear", async function () {
        const abi = JSON.parse('[ { "inputs": [], "name": "test", "outputs": [ { "internalType": "uint256", "name": "", "type": "uint256" } ], "stateMutability": "pure", "type": "function" } ]');
        const ContractInstance = new ethers.Contract(PrimaryWalletContract.address, abi, Owner);

        expect(
            (await ContractInstance.test())
        ).to.equal(1);
    });

    it("Following calls to withdraw should be reverted if signed by non owner account", async function () {
        const [, owner2] = await ethers.getSigners();

        const object_id = "1.36.1";

        await expect(
            PrimaryWalletContract.connect(owner2)['withdraw(address,uint256,string)'](owner2.address, ethers.utils.parseEther("1.0"), object_id)
        ).to.be.revertedWith('Not an owner');

        expect(
            (await PrimaryWalletContract.getBalance())
        ).to.equal(ethers.utils.parseEther("1.0"));
    });

    it("First call to updateOwners should poulate Owners array and remove Owner (single transaction)", async function () {
        const [new_owner1, new_owner2, new_owner3, new_owner4, new_owner5] = await ethers.getSigners();

        const transactions = [{data:"0x23ab6adf000000000000000000000000000000000000000000000000000000000000004000000000000000000000000000000000000000000000000000000000000001a00000000000000000000000000000000000000000000000000000000000000005000000000000000000000000f39Fd6e51aad88F6F4ce6aB8827279cffFb92266000000000000000000000000000000000000000000000000000000000000000100000000000000000000000070997970C51812dc3A010C7d01b50e0d17dc79C800000000000000000000000000000000000000000000000000000000000000010000000000000000000000003C44CdDdB6a900fa2b585dd299e03d12FA4293BC000000000000000000000000000000000000000000000000000000000000000100000000000000000000000090F79bf6EB2c4f870365E785982E1f101E93b906000000000000000000000000000000000000000000000000000000000000000100000000000000000000000015d34AAf54267DB7D7c367839AAf71A00a2C6A6500000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000000000000000006312E33352E300000000000000000000000000000000000000000000000000000", signature:{v:27, r:"0x599318216450293cac667e6e8425d0bdebc8fab28a8ab6ff3290ca3eb9057fb2", s:"0x593e61f2333af9e74cea16e8ccd542a82983439b768c30725130fe5b287ebbd9"}}];
        await PrimaryWalletContract.connect(Owner)['updateOwners((bytes,(uint8,bytes32,bytes32))[])'](transactions);

        expect((await PrimaryWalletContract.getOwner())).to.equal("0x0000000000000000000000000000000000000000");
        let owners = await PrimaryWalletContract.getOwners();
        expect(owners.length).to.equal(5);
        expect(owners[0].address).to.equal(new_owner1.addr);
        expect(owners[1].address).to.equal(new_owner2.addr);
        expect(owners[2].address).to.equal(new_owner3.addr);
        expect(owners[3].address).to.equal(new_owner4.addr);
        expect(owners[4].address).to.equal(new_owner5.addr);
    });

    it("Calls to updateOwners should be reverted if object_id is reused", async function () {
        const [owner1, owner2, owner3, owner4, owner5, new_owner1, new_owner2, new_owner3, new_owner4, new_owner5] = await ethers.getSigners();

        let new_owners = [
            {addr: new_owner1.address, weight: 1},
            {addr: new_owner2.address, weight: 1},
            {addr: new_owner3.address, weight: 1},
            {addr: new_owner4.address, weight: 1},
            {addr: new_owner5.address, weight: 1}
        ];
        const object_id = "1.35.0";

        expect(
            PrimaryWalletContract.connect(owner1)['updateOwners((address,uint256)[],string)'](new_owners, object_id)
        ).to.be.revertedWith('Transaction with this object_id has already been processed');
    });

    it("Following calls to updateOwners should be reverted if signed by non owner account", async function () {
        const [owner1, owner2, owner3, owner4, owner5, new_owner1, new_owner2, new_owner3, new_owner4, new_owner5] = await ethers.getSigners();

        let new_owners = [
            {addr: new_owner1.address, weight: 1},
            {addr: new_owner2.address, weight: 1},
            {addr: new_owner3.address, weight: 1},
            {addr: new_owner4.address, weight: 1},
            {addr: new_owner5.address, weight: 1}
        ];
        const object_id = "1.35.1";

        expect(
            PrimaryWalletContract.connect(new_owner1)['updateOwners((address,uint256)[],string)'](new_owners, object_id)
        ).to.be.revertedWith('Not an owner');;
    });

    it("Following calls to updateOwners should be executed only if they retrieve enough signatures", async function () {
        const [owner1, owner2, owner3, owner4, owner5, new_owner1, new_owner2, new_owner3, new_owner4, new_owner5] = await ethers.getSigners();

        let owners;

        let new_owners = [
            {addr: new_owner1.address, weight: 1},
            {addr: new_owner2.address, weight: 1},
            {addr: new_owner3.address, weight: 1},
            {addr: new_owner4.address, weight: 1},
            {addr: new_owner5.address, weight: 1}
        ];
        const object_id = "1.35.2";

        await PrimaryWalletContract.connect(owner1)['updateOwners((address,uint256)[],string)'](new_owners, object_id);

        expect((await PrimaryWalletContract.getOwner())).to.equal("0x0000000000000000000000000000000000000000");
        owners = await PrimaryWalletContract.getOwners();
        expect(owners.length).to.equal(5);
        expect(owners[0].address).to.equal(owner1.addr);
        expect(owners[1].address).to.equal(owner2.addr);
        expect(owners[2].address).to.equal(owner3.addr);
        expect(owners[3].address).to.equal(owner4.addr);
        expect(owners[4].address).to.equal(owner5.addr);

        await PrimaryWalletContract.connect(owner2)['updateOwners((address,uint256)[],string)'](new_owners, object_id);

        expect((await PrimaryWalletContract.getOwner())).to.equal("0x0000000000000000000000000000000000000000");
        owners = await PrimaryWalletContract.getOwners();
        expect(owners.length).to.equal(5);
        expect(owners[0].address).to.equal(owner1.addr);
        expect(owners[1].address).to.equal(owner2.addr);
        expect(owners[2].address).to.equal(owner3.addr);
        expect(owners[3].address).to.equal(owner4.addr);
        expect(owners[4].address).to.equal(owner5.addr);

        await PrimaryWalletContract.connect(owner3)['updateOwners((address,uint256)[],string)'](new_owners, object_id);

        expect((await PrimaryWalletContract.getOwner())).to.equal("0x0000000000000000000000000000000000000000");
        owners = await PrimaryWalletContract.getOwners();
        expect(owners.length).to.equal(5);
        expect(owners[0].address).to.equal(owner1.addr);
        expect(owners[1].address).to.equal(owner2.addr);
        expect(owners[2].address).to.equal(owner3.addr);
        expect(owners[3].address).to.equal(owner4.addr);
        expect(owners[4].address).to.equal(owner5.addr);

        await PrimaryWalletContract.connect(owner4)['updateOwners((address,uint256)[],string)'](new_owners, object_id);

        expect((await PrimaryWalletContract.getOwner())).to.equal("0x0000000000000000000000000000000000000000");
        owners = await PrimaryWalletContract.getOwners();
        expect(owners.length).to.equal(5);
        expect(owners[0].address).to.equal(new_owner1.addr);
        expect(owners[1].address).to.equal(new_owner2.addr);
        expect(owners[2].address).to.equal(new_owner3.addr);
        expect(owners[3].address).to.equal(new_owner4.addr);
        expect(owners[4].address).to.equal(new_owner5.addr);
    });

});
