// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

interface Proxy {
    function upgradeTo(address newImplementation) external;
}

contract Multisig {
    struct SON {
        address addr;
        uint weight;
    }

    struct SIGNATURE {
        bool processed;
        uint valid_till_block;
        uint[] sig_array;
    }

    uint constant valid_block_numbers = 100;
    uint constant min_owners_count = 1;
    uint constant max_owners_count = 7;
    address public Owner;
    SON[] public Owners;
    mapping(string => SIGNATURE) public Signatures; //object_id = space_id.type_id.instance

    function ownerExist(address _addr) private view returns (bool){
        for (uint i; i< Owners.length; i++){
            if (Owners[i].addr ==_addr)
                return true;
        }

        return false;
    }

    function ownerId(address _addr) private view returns (int){
        for (uint i; i< Owners.length; i++){
            if (Owners[i].addr ==_addr)
                return int(i);
        }

        return -1;
    }

    modifier onlyOwners(address _sender) {
        bool OwnerOnly = ((Owner == _sender) && (Owners.length == 0));
        bool OwnersOnly = ((Owner == address(0)) && (Owners.length > 0) && (ownerExist(_sender)));

        bool allowed = OwnerOnly || OwnersOnly;

        require(allowed, "Not an owner");
        _;
    }

    modifier multisig(address _sender, string memory _object_id) {
        int OwnerId;

        if(Owners.length > 0) {
            if (Signatures[_object_id].sig_array.length == 0) {
                Signatures[_object_id].processed = false;
                Signatures[_object_id].valid_till_block = block.number + valid_block_numbers;
                for (uint i; i< Owners.length; i++){
                    Signatures[_object_id].sig_array.push(0);
                }
            }

            OwnerId = ownerId(_sender);
        }
        else {
            if (Signatures[_object_id].sig_array.length == 0) {
                Signatures[_object_id].processed = false;
                Signatures[_object_id].valid_till_block = block.number + valid_block_numbers;
                Signatures[_object_id].sig_array.push(0);
            }
        }

        require(Signatures[_object_id].processed == false, "Transaction with this object_id has already been processed");
        require(OwnerId > -1, "Wrong owner id");
        require(block.number <= Signatures[_object_id].valid_till_block, "Timeout for transaction signing");
        require(Signatures[_object_id].sig_array[uint(OwnerId)] == 0, "Owner has already sign transaction");
        Signatures[_object_id].sig_array[uint(OwnerId)] = 1;

        uint count = 0;
        for (uint i; i<Signatures[_object_id].sig_array.length; i++) {
            if (Signatures[_object_id].sig_array[i] == 1) {
                count += 1;
            }
        }

        if (uint(3) * count >= Signatures[_object_id].sig_array.length * uint(2)) {
            Signatures[_object_id].processed = true;
            // delete Signatures[_object_id]; -> We don't want to delete procceed transactions
            _;
        }
    }

    function updateOwnersMultisig(address _sender, SON[] memory _newOwners, string memory _object_id) public onlyOwners(_sender) multisig(_sender, _object_id) {
        require(((_newOwners.length >= min_owners_count) && (_newOwners.length <= max_owners_count)), "Invalid number of owners");

        Owner = address(0);
        delete Owners;
        for (uint i; i<_newOwners.length; i++) {
            Owners.push(_newOwners[i]);
        }
    }
}

contract ProxyAdminMultisig is Multisig {
    constructor(address _owner) {
        Owner = _owner;
    }

    function upgrade(address _proxyAddress, address _implementationAddress, string memory _version) public onlyOwners(msg.sender) multisig(msg.sender, _version) {
        Proxy proxy = Proxy(_proxyAddress);
        proxy.upgradeTo(_implementationAddress);
    }
}

contract PrimaryWallet is Multisig, Initializable {
    struct ETH_SIGNATURE {
        uint8 v; 
        bytes32 r;
        bytes32 s;
    }

    struct TRANSACTION {
        bytes data;
        ETH_SIGNATURE signature;
    }

    event NewOwners(SON[] _previousOwners, SON[] _newOwners);
    event Deposit(address _from, uint _value);
    event Withdraw(address _to, uint _value);

    address public ProxyAdmin;

    function initialize(address _owner) external initializer {
        require(address(0) != _owner, "Zero address not allowed");

        Owner = _owner;
        ProxyAdminMultisig c = new ProxyAdminMultisig(_owner);
        ProxyAdmin = address(c);
    }

    function updateOwners(TRANSACTION[] calldata transactions) public virtual {
        require(transactions.length != 0, "No transactions to process");

        bytes32 transaction_hashed_data = keccak256(transactions[0].data);

        for (uint i; i<transactions.length; i++) {
            bytes32 hashed_data = keccak256(transactions[i].data);
            require(transaction_hashed_data == hashed_data, "Transaction data is not the same");

            (SON[] memory newOwners, string memory object_id) = abi.decode(transactions[i].data[4:], (SON[], string));
            address from = ecrecover(hashed_data, transactions[i].signature.v, transactions[i].signature.r, transactions[i].signature.s);

            updateOwners_(from, newOwners, object_id);
        }
    }
    
    function updateOwners(SON[] memory _newOwners, string memory _object_id) public virtual {
        updateOwners_(msg.sender, _newOwners, _object_id);
    }

    function updateOwners_(address _from, SON[] memory _newOwners, string memory _object_id) internal virtual {
        Multisig.updateOwnersMultisig(_from, _newOwners, _object_id);

        ProxyAdminMultisig c = ProxyAdminMultisig(ProxyAdmin);
        c.updateOwnersMultisig(_from, _newOwners, _object_id);

        SON[] memory previousOwners = new SON[](Owners.length);
        for (uint i; i<Owners.length; i++) {
            previousOwners[i] = Owners[i];
        }
        SON[] memory newOwners = new SON[](_newOwners.length);
        for (uint i; i<_newOwners.length; i++) {
            newOwners[i] = _newOwners[i];
        }

        emit NewOwners(previousOwners, newOwners);
    }

    receive() external payable {
        deposit();
    }

    function deposit() public payable virtual {
        emit Deposit(msg.sender, msg.value);
    }

    function withdraw(TRANSACTION[] calldata transactions) public payable virtual {
        require(transactions.length != 0, "No transactions to process");

        bytes32 transaction_hashed_data = keccak256(transactions[0].data);

        for (uint i; i<transactions.length; i++) {
            bytes32 hashed_data = keccak256(transactions[i].data);
            require(transaction_hashed_data == hashed_data, "Transaction data is not the same");

            (address to, uint amount, string memory object_id) = abi.decode(transactions[i].data[4:], (address, uint, string));
            address from = ecrecover(hashed_data, transactions[i].signature.v, transactions[i].signature.r, transactions[i].signature.s);

            withdraw_(from, payable(to), amount, object_id);
        }
    }

    function withdraw(address payable _to, uint _amount, string memory _object_id) public payable virtual {
        withdraw_(msg.sender, _to, _amount, _object_id);
    }

    function withdraw_(address _from, address payable _to, uint _amount, string memory _object_id) internal virtual onlyOwners(_from) multisig(_from, _object_id) {
        _to.transfer(_amount);

        emit Withdraw(_to, _amount);
    }

    function getProxyAdmin() public view virtual returns (address) {
        return ProxyAdmin;
    }

    function getBalance() public view virtual returns (uint) {
        return address(this).balance;
    }

    function getOwner() public view virtual returns (address) {
        return Owner;
    }

    function getOwners() public view virtual returns (SON[] memory) {
        return Owners;
    }

    function getBlockNumber() public view virtual returns (uint) {
        return block.number;
    }

    function getVersion() public pure virtual returns (string memory) {
        return "v1";
    }
}
